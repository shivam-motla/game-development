Blockly.JavaScript['sprite_create'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
  var value_x = Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC);
  var value_y = Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC);
  var value_width = Blockly.JavaScript.valueToCode(block, 'WIDTH', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'HEIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = variable_name +' = createSprite ('+ value_x + ','+ value_y + ',' + value_width + ',' + value_height+');\n';
  return code;
};

Blockly.JavaScript['sprite_shapecolor'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
  var colour_color = block.getFieldValue('COLOR');
  // TODO: Assemble JavaScript into code variable.
  var code = variable_name + '.shapeColor = \"' + colour_color +'\";\n';
  return code;
};

Blockly.JavaScript['sprite_y'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
  var value_val = Blockly.JavaScript.valueToCode(block, 'VAL', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code =  variable_name +'.x = '+ value_val + ';\n';
  return code;
};

Blockly.JavaScript['sprite_x'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
  var value_val = Blockly.JavaScript.valueToCode(block, 'VAL', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code =  variable_name +'.y = '+ value_val + ';\n';
  return code;
};

Blockly.JavaScript['sprite_velocity_x'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
  var value_val = Blockly.JavaScript.valueToCode(block, 'VAL', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code =  variable_name +'.velocity.x = '+ value_val + ';\n';
  return code;
};

Blockly.JavaScript['sprite_velocity_y'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
  var value_val = Blockly.JavaScript.valueToCode(block, 'VAL', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code =  variable_name +'.velocity.y = '+ value_val + ';\n';
  return code;
};

Blockly.JavaScript['sprite_bounce_off'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
  var variable_target = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('target'), Blockly.Variables.NAME_TYPE);
  // TODO: Assemble JavaScript into code variable.
  var code = variable_name + '.bounce('+ variable_target + ');\n';
  return code;
};

Blockly.JavaScript['sprite_draw'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
  // TODO: Assemble JavaScript into code variable.
  var code = 'drawSprite('+ variable_name +');\n';
  return code;
};

Blockly.JavaScript['sprite_draw_all'] = function(block) {
  var code = 'drawSprites( );\n';
  return code;
};

Blockly.JavaScript['assign'] = function(block) {
  var value_a = Blockly.JavaScript.valueToCode(block, 'A', Blockly.JavaScript.ORDER_ATOMIC);
  var value_b = Blockly.JavaScript.valueToCode(block, 'B',  );
  // TODO: Assemble JavaScript into code variable.
  var code = value_a + '=' + value_b + ';\n';
  return code;
};

Blockly.JavaScript['p5_key_went_down'] = function(block) {
  var value_key = Blockly.JavaScript.valueToCode(block, 'KEY', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'keyWentDown(' + value_key +")";
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_key_went_up'] = function(block) {
  var value_key = Blockly.JavaScript.valueToCode(block, 'KEY', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'keyWentUp(' + value_key +")";
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_backspace_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'BACKSPACE';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_delete_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'DELETE';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_enter_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'ENTER';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_return_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'RETURN';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_escape_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'ESCAPE';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_tab_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'TAB';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_shift_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'SHIFT';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_control_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'CONTROL';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_alt_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'ALT';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_option_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'OPTION';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_up_arrow_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'UP_ARROW';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_down_arrow_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'DOWN_ARROW';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_left_arrow_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'LEFT_ARROW';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['p5_right_arrow_key'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'RIGHT_ARROW';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['sprite_is_touching'] = function(block) {
  var variable_sprite = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('SPRITE'), Blockly.Variables.NAME_TYPE);
  var variable_target = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('TARGET'), Blockly.Variables.NAME_TYPE);
  // TODO: Assemble JavaScript into code variable.
  var code = variable_sprite + '.isTouching(' + variable_target + ')';
  return [code, Blockly.JavaScript.ORDER_NONE];
};