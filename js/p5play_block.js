Blockly.Blocks['sprite_create'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldVariable("sprite"), "NAME")
        .appendField("=");
    this.appendDummyInput()
        .appendField("createSprite");
    this.appendValueInput("X")
        .setCheck("Number")
        .appendField("x:");
    this.appendValueInput("Y")
        .setCheck("Number")
        .appendField("y:");
    this.appendValueInput("WIDTH")
        .setCheck("Number")
        .appendField("width:");
    this.appendValueInput("HEIGHT")
        .setCheck("Number")
        .appendField("height:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("http://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-createSprite");
  }
};

Blockly.Blocks['sprite_shapecolor'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldVariable("sprite"), "NAME")
        .appendField(".shapeColor = ")
        .appendField(new Blockly.FieldColour("#ff0000"), "COLOR");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("http://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-createSprite");
  }
};

Blockly.Blocks['sprite_y'] = {
  init: function() {
    this.appendValueInput("VAL")
        .setCheck("Number")
        .appendField(new Blockly.FieldVariable("sprite"), "NAME")
        .appendField(".y =");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("http://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-createSprite");
  }
};

Blockly.Blocks['sprite_x'] = {
  init: function() {
    this.appendValueInput("VAL")
        .setCheck("Number")
        .appendField(new Blockly.FieldVariable("sprite"), "NAME")
        .appendField(".x =");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("http://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-createSprite");
  }
};

Blockly.Blocks['sprite_velocity_x'] = {
  init: function() {
    this.appendValueInput("VAL")
        .setCheck("Number")
        .appendField(new Blockly.FieldVariable("sprite"), "NAME")
        .appendField(".velocityX =");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("http://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-createSprite");
  }
};

Blockly.Blocks['sprite_velocity_y'] = {
  init: function() {
    this.appendValueInput("VAL")
        .setCheck("Number")
        .appendField(new Blockly.FieldVariable("sprite"), "NAME")
        .appendField(".velocityY =");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("http://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-createSprite");
  }
};

Blockly.Blocks['sprite_bounce_off'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldVariable("sprite"), "NAME")
        .appendField(".bounceOff");
    this.appendDummyInput()
        .appendField("(")
        .appendField(new Blockly.FieldVariable("target"), "target")
        .appendField(")");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("http://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-createSprite");
  }
};

Blockly.Blocks['sprite_draw'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("drawSprite (")
        .appendField(new Blockly.FieldVariable("sprite"), "NAME")
        .appendField(")");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("http://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-createSprite");
  }
};

Blockly.Blocks['sprite_draw_all'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("drawSprites( )");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("http://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-createSprite");
  }
};

Blockly.Blocks['assign'] = {
  init: function() {
    this.appendValueInput("A")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("=");
    this.appendValueInput("B")
        .setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['p5_key_went_down'] = {
  init: function() {
    this.appendValueInput("KEY")
        .setCheck("key")
        .appendField("keyWentDown (");
    this.appendDummyInput()
        .appendField(" )");
    this.setInputsInline(true);
    this.setOutput(true, "Boolean");
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("https://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-keyDown");
  }
};

Blockly.Blocks['p5_key_went_up'] = {
  init: function() {
    this.appendValueInput("KEY")
        .setCheck("key")
        .appendField("keyWentUp (");
    this.appendDummyInput()
        .appendField(" )");
    this.setOutput(true, "Boolean");
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("https://molleindustria.github.io/p5.play/docs/classes/p5.play.html#method-keyDown");
  }
};

Blockly.Blocks['p5_backspace_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("BACKSPACE");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_delete_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("DELETE");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_enter_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("ENTER");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_return_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("RETURN");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_tab_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("TAB");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_escape_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("ESCAPE");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_shift_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("SHIFT");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_control_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("CONTROL");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_option_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("OPTION");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_alt_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("ALT");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_up_arrow_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("UP ARROW");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_down_arrow_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("DOWN ARROW");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_left_arrow_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("LEFT ARROW");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};

Blockly.Blocks['p5_right_arrow_key'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("RIGHT ARROW");
    this.setOutput(true, "key");
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("https://p5js.org/reference/#p5/keyCode");
  }
};


Blockly.Blocks['sprite_is_touching'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldVariable("sprite"), "SPRITE")
        .appendField(".isTouching (")
        .appendField(new Blockly.FieldVariable("target"), "TARGET")
        .appendField(" )");
    this.setInputsInline(true);
    this.setOutput(true, "Boolean");
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("https://code-dot-org.github.io/p5.play/docs/classes/Sprite.html#method-isTouching");
  }
};